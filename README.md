# README #

Author: Jacob Brown, jbrown14@uoregon.edu
Project 6, 11/20/2018, for CIS 322, Fall 2018

## Description:

A Flask, MongoDB, RESTful API implementation of an ACP Brevet Control Times Calculator. From the Controle Submission Page, set the Brevet distance, starting time and date, and then set the control location. The program will calculate the opening and closing times using these values and store them in a database. You can then view these times on the display page, in JSON or CSV format, or on a php formatted page.

## Instructions:

(Docker) Use start.sh to build and run the webapp service, laptop service, and database service:

[Webapp Service]: Go to "localhost:5000" in a browser to see the submission form page. Enter the values, then click "Submit." Click on "Display" to see all of your entries calculated with the correct opening and closing times.

[Laptop Service]: Go to "localhost:5001" and the paths described in "API Paths" to view the database entries in either JSON format or CSV format.

[Website Service]: Go to "localhost:5002" and click on "List All", "List Open Only", or "List Close Only" to specify how much of each database entry you would like to view.

NOTE - if you run into any difficulties attempting to use "start.sh" again, try using "rebuild.sh" instead. This will force a rebuild of every service in docker-compose.yml.

## API Paths

The following will display every entry in JSON format:

- http://localhost:5001/listAll - displays open and close times
- http://localhost:5001/listOpenOnly - displays open times only
- http://localhost:5001/listCloseOnly - displays close times only

- http://localhost:5001/listAll/json - displays open and close times
- http://localhost:5001/listOpenOnly/json - displays open times only
- http://localhost:5001/listCloseOnly/json - displays close times only

The following will display every entry in CSV format:

- http://localhost:5001/listAll/csv - displays open and close times
- http://localhost:5001/listOpenOnly/csv - displays open times only
- http://localhost:5001/listCloseOnly/csv - displays close times only

The following will display "k" number of entries in ascending order:

- http://localhost:5001/listOpenOnly/json?top=3 - displays the top 3 open times in JSON format
- http://localhost:5001/listCloseOnly/json?top=4 - displays the top 4 close times in JSON format
- http://localhost:5001/listOpenOnly/csv?top=3 - displays the top 3 open times in CSV format
- http://localhost:5001/listCloseOnly/csv?top=4 - displays the top 4 close times in CSV format

## Testing

Although no testing programs exist for this project, you can try the following scenarios yourself:

(Empty Database) Without submitting anything yet, click on the "Display All" button. You will be routed to the an empty display page with the message "WARNING: Empty database. Go back and try again."

(No Control Entered) Go to the submission page and click the "Submit" button without entering anything in the "Control Distance (km)" field. You will be routed to an error page that says "ERROR: No control distance was entered"

(Unreasonable Control Distance) Enter a control distance greater than 1300 (the maximum distance of a brevet) to see the message "ERROR: Control distance is greater than brevet maximum distance"

(Incorrect Path) From the laptop service (localhost:5001), enter an incorrect path (such as localhost:5001/listCloseOnly/json?top=wrong) to see the all the entries by default.

(Top Zero Entries) From the laptop service (localhost:5001), enter "0" when trying to display the top "k" entries in the database. By default, MongoDB will return ALL entries. However, this has been adjusted to return NO entries.
