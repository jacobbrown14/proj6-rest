<!-- index.php - Edited by Jacob Brown, 11/20/2018    -->
<!-- for Project 6, CIS 322, Fall 2018                -->
<html>
    <head>
        <title>CIS 322 REST-api demo: Open/Close Control Times</title>
    </head>

    <body>
        <h1>CIS 322 REST-api Demo: Open/Close Control Times</h1>
        <h3>Press a button to see a list of control times</h3>

        <form action="" method="POST">
            <input type="submit" id="listAll" name="listAll" class="btn-info" value="List All">
            <input type="submit" id="listOpenOnly" name="listOpenOnly" class="btn-info" value="List Open Only">
            <input type="submit" id="listCloseOnly" name="listCloseOnly" class="btn-info" value="List Close Only">
        </form>
        <hr width=55% align=left></hr>
        <ul>
            <?php
 
            if (isset($_POST['listAll'])){
                $json = file_get_contents('http://laptop-service/listAll');
                $obj = json_decode($json);
	            $list = $obj->ListAll;
                foreach ($list as $l) {
                    echo "<li>Control (km): $l->control</li>";
                    echo "<li>Distance (km): $l->distance</li>";
                    echo "<li>Open: $l->control_open</li>";
                    echo "<li>Close: $l->control_close</li>";
                    echo "<hr width=50% align=left>";
                }
            } else if (isset($_POST['listOpenOnly'])){
                $json = file_get_contents('http://laptop-service/listOpenOnly');
                $obj = json_decode($json);
                    $list = $obj->ListOpenOnly;
                foreach ($list as $l) {
                    echo "<li>Control (km): $l->control</li>";
                    echo "<li>Distance (km): $l->distance</li>";
                    echo "<li>Open: $l->control_open</li>";
                    echo "<hr width=50% align=left>";
                }
            } else if (isset($_POST['listCloseOnly'])){
                $json = file_get_contents('http://laptop-service/listCloseOnly');
                $obj = json_decode($json);
                    $list = $obj->ListCloseOnly;
                foreach ($list as $l) {
                    echo "<li>Control (km): $l->control</li>";
                    echo "<li>Distance (km): $l->distance</li>";
                    echo "<li>Close: $l->control_close</li>";
                    echo "<hr width=50% align=left>";
                }
            }
            ?>
        </ul>
    </body>
</html>

