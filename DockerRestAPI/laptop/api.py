"""
REST API used to expose what's in our database in
either CSV or JSON format

Edited by Jacob Brown, 11/20/2018
for Project 6, CIS 322, Fall 2018

CSV formatting idea from:
https://stackoverflow.com/questions/9157314/how-do-i-write-data-into-csv-format-as-string-not-file
"""
import os, io, csv
from flask import Flask, jsonify
from flask_restful import Resource, Api, request
from pymongo import MongoClient

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tracks

# getListAllJSON() - returns all entries in db
# in handy JSON format
def getListAllJSON():
    
    dict = []
    getta = db.collection

    for i in getta.find():
        dict.append({
            'control' : i['control'],
            'distance' : i['distance'],
            'control_open' : i['control_open'],
            'control_close' : i['control_close']
        })

    list_all = {'ListAll' : dict}
    return jsonify(list_all)

# getListAllCSV() - returns all entries in db
# in handy CSV format
def getListAllCSV():

    list_all = io.StringIO()
    dict = []
    getta = db.collection

    for i in getta.find():
        dict.append({
            'control' : i['control'],
            'distance' : i['distance'],
            'control_open' : i['control_open'],
            'control_close' : i['control_close']
        })

    writer = csv.writer(list_all, quoting=csv.QUOTE_NONNUMERIC)
    writer.writerow(dict)

    # Tweaking the formatting of the CSV string a bit...
    csv_str = str(list_all.getvalue())
    temp1 = csv_str.replace('\"', '')
    temp2 = temp1.replace(',{', ', {')
    new_str = temp2.replace('\r\n', '')

    return new_str

# getListOpenOnlyJSON() - returns open times
# of all entries in JSON format
def getListOpenOnlyJSON():

    dict = []
    getta = db.collection

    for i in getta.find():
        dict.append({
            'control' : i['control'],
            'distance' : i['distance'],
            'control_open' : i['control_open']
        })

    list_open_only = {"ListOpenOnly" : dict}
    return jsonify(list_open_only)

# getListOpenOnlyCSV() - returns open times
# of all entries in CSV format
def getListOpenOnlyCSV():

    list_open = io.StringIO()
    dict = []
    getta = db.collection

    for i in getta.find():
        dict.append({
            'control' : i['control'],
            'distance' : i['distance'],
            'control_open' : i['control_open'],
        })

    writer = csv.writer(list_open, quoting=csv.QUOTE_NONNUMERIC)
    writer.writerow(dict)

    # Tweaking the formatting of the CSV string a bit...
    new_str = formatCSVString(str(list_open.getvalue()))

    return new_str

# getListCloseOnlyJSON() - returns close times
# of all entries in JSON format
def getListCloseOnlyJSON():

    dict = []
    getta = db.collection

    for i in getta.find():
        dict.append({            
            'control' : i['control'],
            'distance' : i['distance'],
            'control_close' : i['control_close']
        })

    list_close_only = {"ListCloseOnly" : dict}
    return jsonify(list_close_only)

# getListCloseOnlyCSV() - returns close times
# of all entries in CSV format
def getListCloseOnlyCSV():

    list_close = io.StringIO()
    dict = []
    getta = db.collection

    for i in getta.find():
        dict.append({
            'control' : i['control'],
            'distance' : i['distance'],
            'control_close' : i['control_close'],
        })

    writer = csv.writer(list_close, quoting=csv.QUOTE_NONNUMERIC)
    writer.writerow(dict)

    # Tweaking the formatting of the CSV string a bit...
    new_str = formatCSVString(str(list_close.getvalue()))

    return new_str

# getListOpenOnlyTopJSON(k) - returns 'k' number
# of sorted open times in JSON format
def getListOpenOnlyTopJSON(k):

    dict = []
    getta = db.collection
    
    # Since limit(0) returns all, this fixes that and returns nothing
    if k != 0:
        for i in getta.find().sort([('control_open', 1)]).limit(k):
            dict.append({
                'control' : i['control'],
                'distance' : i['distance'],
                'control_open' : i['control_open']
            })

    list_open_top = {"ListOpenOnlyTop" : dict}
    return jsonify(list_open_top)

# getListOpenOnlyTopCSV(k) - returns 'k' number
# of sorted open times in CSV format
def getListOpenOnlyTopCSV(k):

    list_open = io.StringIO()
    new_str = ""

    dict = []
    getta = db.collection

    # Since limit(0) returns all, this fixes that and returns nothing
    if k != 0:
        for i in getta.find().sort([('control_open', 1)]).limit(k):
            dict.append({
                'control' : i['control'],
                'distance' : i['distance'],
                'control_open' : i['control_open'],
            })

        writer = csv.writer(list_open, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(dict)

        # Tweaking the formatting of the CSV string a bit...
        new_str = formatCSVString(str(list_open.getvalue()))

    return new_str

# getListCloseOnlyTopJSON(k) - returns 'k' number
# of sorted close times in JSON format
def getListCloseOnlyTopJSON(k):
    
    dict = []
    getta = db.collection

    # Since limit(0) returns all, this fixes that and returns nothing
    if k != 0:
        for i in getta.find().sort([('control_close', 1)]).limit(k):
            dict.append({
                'control' : i['control'],
                'distance' : i['distance'],
                'control_close' : i['control_close']
            })

    list_close_top = {"ListCloseOnlyTop" : dict}
    return jsonify(list_close_top)

# getListCloseOnlyTopCSV(k) - returns 'k' number
# of sorted close times in CSV format
def getListCloseOnlyTopCSV(k):

    list_close = io.StringIO()
    new_str = ""

    dict = []
    getta = db.collection

    # Since limit(0) returns all, this fixes that and returns nothing
    if k != 0:
        for i in getta.find().sort([('control_open', 1)]).limit(k):
            dict.append({
                'control' : i['control'],
                'distance' : i['distance'],
                'control_close' : i['control_close'],
            })

        writer = csv.writer(list_close, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(dict)

        # Tweaking the formatting of the CSV string a bit...
        new_str = formatCSVString(str(list_close.getvalue()))

    return new_str

# formatCSVString(str) - helper function that formats
# a string to something closer to CSV format
def formatCSVString(str):
   temp1 = str.replace('\"', '')
   temp2 = temp1.replace(',{', ', {')
   new_str = temp2.replace('\r\n', '')
   return new_str

# Classes for each path
class Laptop(Resource):
    def get(self):
        return "NOTHING HERE BEAT IT"

class ListAll(Resource):
    def get(self):
        return getListAllJSON()

class ListAllParam(Resource):
    def get(self, param):
        if param == "csv":
            return getListAllCSV()
        elif param == "json":
            return getListAllJSON()
        
        return "Invalid path (listAll)"

class ListOpenOnly(Resource):
    def get(self):
        return getListOpenOnlyJSON()

class ListOpenOnlyParam(Resource):
    def get(self, param):
        k = request.args.get('top', type=int)

        # Makes sure we're passing an int
        if isinstance(k, int):
            if param == "csv":
                return getListOpenOnlyTopCSV(k)
            elif param == "json":
                return getListOpenOnlyTopJSON(k)

        # If we're not dealing with top entries,
        # we just return something in csv or JSON
        if param == "csv":
            return getListOpenOnlyCSV()
        elif param == "json":
            return getListOpenOnlyJSON()

        return "Invalid path (listOpenOnly)"

class ListCloseOnly(Resource):
    def get(self):
        return getListCloseOnlyJSON()

class ListCloseOnlyParam(Resource):
    def get(self, param):
        k = request.args.get('top', type=int)

        # Makes sure we're passing an int
        if isinstance(k, int):
            if param == "csv":
                return getListCloseOnlyTopCSV(k)
            elif param == "json":
                return getListCloseOnlyTopJSON(k)

        # If we're not dealing with top entries,
        # we just return something in csv or JSON
        if param == "csv":
            return getListCloseOnlyCSV()
        elif param == "json":
            return getListCloseOnlyJSON()

        return "Invalid path (listCloseOnly)"

# Creates routes
api.add_resource(Laptop, '/')
api.add_resource(ListAll, '/listAll', '/listAll/')
api.add_resource(ListAllParam, '/listAll/<string:param>')
api.add_resource(ListOpenOnly, '/listOpenOnly', '/listOpenOnly/')
api.add_resource(ListOpenOnlyParam, '/listOpenOnly/<string:param>')
api.add_resource(ListCloseOnly, '/listCloseOnly', '/listCloseOnly/')
api.add_resource(ListCloseOnlyParam, '/listCloseOnly/<string:param>')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
